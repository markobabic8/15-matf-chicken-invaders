# Project 15-MATF-Chicken-Invaders

Implementacija uproscene verzije popularne igre Chicken Invaders.

## Developers

- [Marko Babic, 77/2017](https://gitlab.com/markobabic8)
- [Maja Crnomarkovic, 21/2017](https://gitlab.com/crnomarkovicm)
- [Aleksandar Milosevic, 60/2017](https://gitlab.com/duhizjame)
- [Aleksandar Lisov, 236/2017](https://gitlab.com/AleksandarLisov)
